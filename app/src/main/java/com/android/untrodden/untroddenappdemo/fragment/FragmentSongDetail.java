package com.android.untrodden.untroddenappdemo.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.untrodden.untroddenappdemo.R;
import com.android.untrodden.untroddenappdemo.model.Results;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by IXEET pc9 on 7/21/2017.
 */

public class FragmentSongDetail extends Fragment {
    private ImageView ivProjectLogo;
    private TextView tvTrackName, tvArtistName, tvGenere, tvPrice, tvDuration;
    private ProgressBar pbProjectLogo;
    private Results resultData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artist_detail, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resultData = getArguments().getParcelable("resultData");
        setData();

    }

    private void initView(View view) {
        tvTrackName = (TextView) view.findViewById(R.id.tv_track_name);
        tvArtistName = (TextView) view.findViewById(R.id.tv_artist_name);
        tvGenere = (TextView) view.findViewById(R.id.tv_genere);
        tvDuration = (TextView) view.findViewById(R.id.tv_duration);
        tvPrice = (TextView) view.findViewById(R.id.tv_price);
        ivProjectLogo = (ImageView) view.findViewById(R.id.iv_song_img);
        pbProjectLogo = (ProgressBar) view.findViewById(R.id.pb_song_img);
    }

    private void setData() {
        tvArtistName.setText("Artist Name :- " + resultData.getArtistName());
        tvTrackName.setText("Track Name :- " + resultData.getTrackName());
        tvDuration.setText("Duration :- " + (int) ((Long.valueOf(resultData.getTrackTimeMillis()) / (1000 * 60)) % 60) + " min");
        tvPrice.setText("Price :- " + resultData.getCollectionPrice() + " Rupee");
        tvGenere.setText("Genere :- " + resultData.getPrimaryGenreName());
        Picasso.with(getActivity()).load(resultData.getArtworkUrl60()).into(ivProjectLogo, new Callback() {
            @Override
            public void onSuccess() {
                pbProjectLogo.setVisibility(View.GONE);
                ivProjectLogo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });
    }
}
