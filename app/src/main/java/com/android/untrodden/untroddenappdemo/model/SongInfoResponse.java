package com.android.untrodden.untroddenappdemo.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IXEET pc9 on 7/21/2017.
 */

public class SongInfoResponse {
    private ArrayList<Results> results;

    private String resultCount;

    public ArrayList<Results> getResults() {
        return results;
    }

    public void setResults(ArrayList<Results> results) {
        this.results = results;
    }

    public String getResultCount() {
        return resultCount;
    }

    public void setResultCount(String resultCount) {
        this.resultCount = resultCount;
    }

    @Override
    public String toString() {
        return "ClassPojo [results = " + results + ", resultCount = " + resultCount + "]";
    }
}
