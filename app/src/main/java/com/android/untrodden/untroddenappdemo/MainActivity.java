package com.android.untrodden.untroddenappdemo;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.untrodden.untroddenappdemo.adapter.AdapterArtistList;
import com.android.untrodden.untroddenappdemo.fragment.FragmentSongDetail;
import com.android.untrodden.untroddenappdemo.model.Results;
import com.android.untrodden.untroddenappdemo.model.SongInfoResponse;
import com.android.untrodden.untroddenappdemo.network.ApiClient;
import com.android.untrodden.untroddenappdemo.network.ApiInterface;
import com.android.untrodden.untroddenappdemo.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterArtistList.onItemClicked {
    private RecyclerView rvList;
    private EditText etSearch;
    private ImageView ivSubmit;
    private ProgressBar pbList;
    private ArrayList<Results> listArtistInfo;
    private AdapterArtistList adapterArtistList;
    private FrameLayout flSearchDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListener();
        setAdapter();
    }

    private void initView() {
        etSearch = (EditText) findViewById(R.id.et_search_list);
        ivSubmit = (ImageView) findViewById(R.id.iv_submit);
        pbList = (ProgressBar) findViewById(R.id.pb_search_list);
        rvList = (RecyclerView) findViewById(R.id.rv_search_list);
        flSearchDetail = (FrameLayout) findViewById(R.id.fl_search_detail);
    }

    private void initListener() {
        ivSubmit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (AppController.isConnected) {
                                                String text = etSearch.getText().toString();
                                                if (text.length() > 1) {
                                                    pbList.setVisibility(View.VISIBLE);
                                                    rvList.setVisibility(View.GONE);
                                                    getArtistInfo(text);
                                                    Utility.hideSoftKeyboard(MainActivity.this);
                                                }
                                            } else {
                                                Toast.makeText(MainActivity.this, "Check your Internet Connection", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

        );
        etSearch.addTextChangedListener(new

                                                TextWatcher() {
                                                    @Override
                                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                                    }

                                                    @Override
                                                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                                    }

                                                    @Override
                                                    public void afterTextChanged(Editable editable) {
                                                        if (editable.length() > 0) {
                                                            ivSubmit.setVisibility(View.VISIBLE);
                                                        } else {
                                                            ivSubmit.setVisibility(View.GONE);
                                                        }
                                                    }
                                                }

        );

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener()

                                           {
                                               @Override
                                               public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                                   if (actionId == EditorInfo.IME_ACTION_DONE | actionId == EditorInfo.IME_ACTION_NEXT) {
                                                       ivSubmit.performClick();
                                                   }
                                                   return false;
                                               }
                                           }

        );
    }

    private void setAdapter() {
        listArtistInfo = new ArrayList<>();
        adapterArtistList = new AdapterArtistList(this, listArtistInfo);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setAdapter(adapterArtistList);
    }

    private void getArtistInfo(String value) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<SongInfoResponse> call = apiService.getSongInfo(value);
        call.enqueue(new Callback<SongInfoResponse>() {
            @Override
            public void onResponse(Call<SongInfoResponse> call, Response<SongInfoResponse> response) {
                ArrayList<Results> listArtistInfo = response.body().getResults();
                setArtistInfo(listArtistInfo);
            }

            @Override
            public void onFailure(Call<SongInfoResponse> call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    private void setArtistInfo(ArrayList<Results> listArtistInfo) {
        pbList.setVisibility(View.GONE);
        rvList.setVisibility(View.VISIBLE);
        this.listArtistInfo.clear();
        this.listArtistInfo.addAll(listArtistInfo);
        adapterArtistList.notifyDataSetChanged();
    }

    @Override
    public void onListItemClicked(Results resultData) {
        loadDetailFragment(resultData);
        flSearchDetail.setVisibility(View.VISIBLE);
    }

    private void loadDetailFragment(Results resultData) {
        FragmentSongDetail fragmentSongDetail = new FragmentSongDetail();
        Bundle bundle = new Bundle();
        bundle.putParcelable("resultData", resultData);
        fragmentSongDetail.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_search_detail, fragmentSongDetail, "fragmentSongDetail").commit();
    }

    @Override
    public void onBackPressed() {
        if (flSearchDetail.getVisibility() == View.VISIBLE) {
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("fragmentSongDetail"));
            flSearchDetail.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
}
