package com.android.untrodden.untroddenappdemo.network;

import com.android.untrodden.untroddenappdemo.model.SongInfoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by IXEET pc9 on 7/21/2017.
 */

public interface ApiInterface {
    @GET("search")
    Call<SongInfoResponse> getSongInfo(@Query("term") String value);
}
