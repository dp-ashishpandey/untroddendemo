package com.android.untrodden.untroddenappdemo.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.untrodden.untroddenappdemo.R;
import com.android.untrodden.untroddenappdemo.model.Results;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IXEET pc9 on 7/21/2017.
 */

public class AdapterArtistList extends RecyclerView.Adapter<AdapterArtistList.MyViewHolderList> {
    Context mContext;
    ArrayList<Results> listArtistInfo;
    onItemClicked onItemClicked;


    public class MyViewHolderList extends RecyclerView.ViewHolder {
        private ImageView ivProjectLogo;
        private TextView tvTrackName, tvArtistName, tvGenere, tvPrice, tvDuration;
        private ProgressBar pbProjectLogo;

        public MyViewHolderList(View itemView) {
            super(itemView);
            tvTrackName = (TextView) itemView.findViewById(R.id.tv_track_name);
            tvArtistName = (TextView) itemView.findViewById(R.id.tv_artist_name);
            tvGenere = (TextView) itemView.findViewById(R.id.tv_genere);
            tvDuration = (TextView) itemView.findViewById(R.id.tv_duration);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            ivProjectLogo = (ImageView) itemView.findViewById(R.id.iv_song_img);
            pbProjectLogo = (ProgressBar) itemView.findViewById(R.id.pb_song_img);
        }
    }

    public AdapterArtistList(Context context, ArrayList<Results> listArtistInfo) {
        this.mContext = context;
        this.listArtistInfo = listArtistInfo;
        onItemClicked = (onItemClicked) mContext;
    }

    @Override
    public MyViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_artist_info_list_item, parent, false);
        return new MyViewHolderList(view);
    }


    @Override
    public void onBindViewHolder(final MyViewHolderList holder, final int position) {
        final Results resultData = listArtistInfo.get(position);
        holder.tvArtistName.setText("Artist Name :- " + resultData.getArtistName());
        holder.tvTrackName.setText("Track Name :- " + resultData.getTrackName());
        holder.tvDuration.setText("Duration :- " + (int) ((Long.valueOf(resultData.getTrackTimeMillis()) / (1000 * 60)) % 60) + " min");
        holder.tvPrice.setText("Price :- " + resultData.getCollectionPrice() + " Rupee");
        holder.tvGenere.setText("Genere :- " + resultData.getPrimaryGenreName());
        Picasso.with(mContext).load(resultData.getArtworkUrl60()).into(holder.ivProjectLogo, new Callback() {
            @Override
            public void onSuccess() {
                holder.pbProjectLogo.setVisibility(View.GONE);
                holder.ivProjectLogo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClicked.onListItemClicked(resultData);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listArtistInfo.size();
    }


    public interface onItemClicked {
        void onListItemClicked(Results resultData);
    }
}